// Navs

$(function() {
    var pull = $('.navbar-toggle');
    var menu = $(pull.attr("data-target"));

    $(pull).on('click', function(e) {
        e.preventDefault();
        pull.toggleClass('active');
        menu.slideToggle(100);
    });

    $('.btn-close').click(function(){
        var tab = $(this).closest('.nav-block');
        var btn = $(this).closest('.nav').find('.navbar-toggle');
        tab.hide();
        btn.removeClass('active');
    });
});


$('.slider').slick({
    arrows: false,
    autoplay: true,
    dots: true,
    vertical: true,
    mobileFirst: true,
    slidesToShow: 1,
    slidesToScroll: 1
});



$('.review').slick({
    arrows: false,
    autoplay: false,
    dots: true,
    vertical: true,
    mobileFirst: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    asNavFor: '.review-image'
});

$('.review-image').slick({
    arrows: false,
    autoplay: false,
    dots: false,
    vertical: true,
    mobileFirst: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    asNavFor: '.review'
});

$('.slider-for').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.slider-nav'
});
$('.slider-nav').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    asNavFor: '.slider-for',
    dots: true,
    centerMode: true,
    focusOnSelect: true
});


$(function() {
    $(window).resize(function() {

        if ($(window).width() > 1024) {
            $('.review-inner').width($(window).width() / 2);
        }
    });

    $(window).resize();
});


$(".btn-modal").fancybox({
    'padding' : 0,
    'maxWidth'   : 760,
    'closeBtn' : false
});

$('.btn-close').click(function(){
    $.fancybox.close();
});


// Чекбоксы

$(".robe-colors input[type='radio']").ionCheckRadio();
$(".params-fonts input[type='radio']").ionCheckRadio();
$(".params-colors input[type='radio']").ionCheckRadio();
$(".params-size input[type='radio']").ionCheckRadio();
$(".robe-type input[type='radio']").ionCheckRadio();

//tabs


$('.tabs-nav a').click(function(e) {
    e.preventDefault();
    var tab = $($(this).attr("data-target"));
    var box = $(this).closest('.tabs');

    console.log(tab);
    $(this).closest('.tabs').find('.tabs-nav').removeClass('active');
    $(this).closest('.tabs-nav').addClass('active');

    box.find('.tabs-item').removeClass('active');
    box.find(tab).addClass('active');
});


/* Калькулятор */
/* ----------- */


// Подбор по полу и возрасту

$('.robe-type label').click(function(e) {
    var container = $(this).closest('.product');
    var value = $($(this).closest('li').attr("data-target"));
    var tab = $(this).find('input[name="robe-type"]').val();

    container.find('.product-item').removeClass('active');
    container.find(value).addClass('active');

    console.log(tab);
    if (tab == '4') {
        container.find('.params-row').addClass('double');
    }
    else {
        container.find('.params-row').removeClass('double');
    }
});



$('.gender-switch li a').click(function(e) {
    e.preventDefault();
    var tab = $($(this).attr("data-target"));
    var box = $(this).closest('.params-row');

    $(this).closest('.gender-switch').find('li').removeClass('active');
    $(this).closest('li').addClass('active');

    box.find('.params-col').removeClass('current');
    box.find(tab).addClass('current');
});



// Цвет вышивки

$('.params-col-one .params-colors li').click(function(e) {
    var container = $(this).closest('.product');
    var value =  $(this).attr("data-target");
    container.find('.print-text').css("color", value);
    container.find('.print-images').css("fill", value);
});

$('.params-col-two .params-colors li').click(function(e) {
    var container = $(this).closest('.product');
    var value =  $(this).attr("data-target");
    container.find('.print-text-woman').css("color", value);
    container.find('.print-images-woman').css("fill", value);
});


// Шрифт

$('.params-col-one .params-fonts .font-item').click(function(e) {
    var container = $(this).closest('.product');
    var value =  ($(this).attr("data-target"));
    container.find('.print-text').css("font-family", value);
});


$('.params-col-two .params-fonts .font-item').click(function(e) {
    var container = $(this).closest('.product');
    var value =  ($(this).attr("data-target"));
    container.find('.print-text-woman').css("font-family", value);
});


// Изображение

$('.params-col-one .params-image label').click(function(e) {
    var container = $(this).closest('.product').find('.print-images');
    var value =  ($(this).attr("data-target"));
    container.find('.print-image-item').removeClass('active');
    container.find(value).addClass('active');
});

$('.params-col-two .params-image label').click(function(e) {
    var container = $(this).closest('.product').find('.print-images-woman');
    var value =  ($(this).attr("data-target"));
    container.find('.print-image-item').removeClass('active');
    container.find(value).addClass('active');
});



$('.params-col-one .image-more span').click(function(e) {
    e.preventDefault();
    var container = $(this).closest('.product').find('.print-images');
    console.log(container);
    container.find('.print-image-item').removeClass('active');
});

$('.params-col-two .image-more span').click(function(e) {
    e.preventDefault();
    var container = $(this).closest('.product').find('.print-images-woman');
    container.find('.print-image-item').removeClass('active');
});

// Текст надписи

$('.params-form').find('textarea[name=params-text-one]').on('input change', function () {
    var value = ($(this).val());
    var container1 = $(this).closest('.product').find('.front');
    var container2 = $(this).closest('.product').find('.back');

    value = value.replace(/\n/ig, '<br>');

    container2.find('.print-text-source').html(value);
    container1.find('.print-text-source').html(value);
});


$('.params-form').find('textarea[name=params-text-two]').on('input change', function () {
    var value = ($(this).val());
    var container1 = $(this).closest('.product').find('.front');
    var container2 = $(this).closest('.product').find('.back');

    value = value.replace(/\n/ig, '<br>');

    container2.find('.print-text-source-woman').html(value);
    container1.find('.print-text-source-woman').html(value);
});

// Цвет халата

$('.robe-colors li').click(function(e) {
    var container = $(this).closest('.product');
    var value =  ($(this).attr("data-target"));

    console.log(value);

    container.find('.robe-color').removeClass('active');
    container.find(value).addClass('active');
});

// Вид халата

$('.view-side').click(function(e) {
    e.preventDefault();
    var container = $(this).closest('.product');
    container.find('.product-visual').toggleClass('switch');
});


$('.product-switch').click(function(e) {
    e.preventDefault();
    var container = $(this).closest('.product');
    container.toggleClass('product-switch-marker');
});


// Променять местами

$('.view-choice').click(function(e) {
    e.preventDefault();
    var container = $(this).closest('.product-visual');
    container.find('.back-print').toggleClass('reverse');
    container.find('.back-print-woman').toggleClass('reverse');
});



// Форма

$('.btn-next').click(function() {

    var container = $(this).closest('.product-params');

    container.find('.params').hide();
    container.find('.order').show();

    $('body').find('form:not(this)').children('div').removeClass('red'); //удаление всех сообщение об ошибке(валидатора)
    var answer = checkForm($(this).closest('form').get(0)); //ответ от валидатора
    if(answer != false)
    {
        var $form = $(this).closest('form'),
            type    =     $('input[name="type"]', $form).val(),
            name    =     $('input[name="name"]', $form).val(),
            phone   =     $('input[name="phone"]', $form).val(),
            email   =     $('input[name="email"]', $form).val(),
            message =     $('textarea[name="message"]', $form).val();
        console.log(name, phone, type, email, message);
        $.ajax({
            type: "POST",
            url: "form-handler.php",
            data: {name: name, phone: phone, email:email, type:type, message:message}
        }).done(function(msg) {
            $('form').find('input[type=text], textarea').val('');
            console.log('удачно');
            document.location.href = "http://virtuactions.ru/done.html";
        });
    }
});

// Коррекция высоты блока подбора

$(function() {
    $(window).resize(function() {

        var w1 = $(window).width();
        var h1 =  $('.product-block').height();
        var h2;


        console.log(w1);

        if (w1 > 1600) {
            h2 = 850 - h1 + 'px';

            console.log(h2);

            $('.b03').css("margin-top", h2);
        }



    });
    $(window).resize();
});


(function($) {
    $.fn.textfill = function(options) {
        var fontSize = options.maxFontPixels;
        var ourText = $('span:visible:first', this);
        var maxHeight = $(this).height();
        var maxWidth = $(this).width();
        var textHeight;
        var textWidth;
        do {
            ourText.css('font-size', fontSize);
            textHeight = ourText.height();
            textWidth = ourText.width();
            fontSize = fontSize - 1;
        } while ((textHeight > maxHeight || textWidth > maxWidth) && fontSize > 3);
        return this;
    }
})(jQuery);

$(document).ready(function() {
    $('.params-col-one .params-text-value')
        .on('keypress', function(e) {
            //console.log('key');
            $('.active .back-print .print-text').textfill({ maxFontPixels: 50 });
        })
        .on('keydown', function(e) {
            $('.active .back-print .print-text').textfill({ maxFontPixels: 50 });
        })
});


$(document).ready(function() {
    $('.params-col-two .params-text-value')

        .on('keypress', function(e) {
            //console.log('key');
            $('.active .back-print-woman .print-text-woman').textfill({ maxFontPixels: 40 });
        })
        .on('keydown', function(e) {
            $('.active .back-print-woman .print-text-woman').textfill({ maxFontPixels: 40 });
        });
});


$(function () {
    $('.select-style').ikSelect({
        autoWidth: false,
        ddFullWidth: false,
        dynamicWidth: false,
        equalWidths: true,
        extractLink: false,
        linkCustomClass: '',
        ddCustomClass: '',
        filter: false,
        ddMaxHeight: 300,
        customClass: 'select-main'
    });
});


// Подключние Яндекс-Карты

ymaps.ready(init);

function init () {
    var myMap = new ymaps.Map("map1", {
            center: [45.0122,41.9082],
            zoom: 14
        }),

    // Создаем геообъект с типом геометрии "Точка".
        myGeoObject = new ymaps.GeoObject({
            // Описание геометрии.
            geometry: {
                type: "Point",
                coordinates: [55.8, 37.8]
            },
            // Свойства.
            properties: {
                // Контент метки.
                iconContent: '',
                hintContent: ''
            }
        }, {
            // Опции.
            // Иконка метки будет растягиваться под размер ее содержимого.
            preset: 'islands#blackStretchyIcon',
            // Метку можно перемещать.
            draggable: true
        });

    myMap.behaviors.disable('scrollZoom');


    myMap.geoObjects
        .add(myGeoObject)
        .add(new ymaps.Placemark([45.0121,41.9154], {
            balloonContent: '<strong>ул. Пирогова 18/3</strong>'
        }, {
            preset: 'islands#icon',
            iconColor: '#0095b6'
        }))
        .add(new ymaps.Placemark([45.0122,41.9082], {
            balloonContent: '<strong>ул. Тухачевского 20/8</strong>'
        }, {
            preset: 'islands#icon',
            iconColor: '#735184'
        }))
        .add(new ymaps.Placemark([45.0168,41.9038], {
            balloonContent: '<strong>ул. Тухачевского 21/3</strong>'
        }, {
            preset: 'islands#icon',
            iconColor: '#3caa3c'
        }))
        .add(new ymaps.Placemark([45.0028,41.9025], {
            balloonContent: '<strong>ул. Родосская 9</strong>'
        }, {
            preset: 'islands#icon',
            iconColor: '#330055'
        }))
        .add(new ymaps.Placemark([45.0151,41.9023], {
            balloonContent: '<strong>ул. Тухачевского 26/5</strong>'
        }, {
            preset: 'islands#icon',
            iconColor: '#a5260a'
        }));
}
